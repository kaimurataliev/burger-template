import React from 'react';
import './NavigationItems.css';
import NavigationItem from './NavItem/NavigationItem';

const NavigationItems = () => (
    <ul className="NavigationItems">
        <NavigationItem to="/" exact>Burger builder</NavigationItem>
        <NavigationItem to="/orders" exact >Orders</NavigationItem>
    </ul>
);

export default NavigationItems;