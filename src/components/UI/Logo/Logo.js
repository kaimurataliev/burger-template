import React from 'react';
import './Logo.css';
import burgerLogo from '../../../assets/images/burger_logo.png';

const Logo = () => {
    return (
        <div className="Logo">
            <img src={burgerLogo} alt="BurgerLogo" />
        </div>
    )
};

export default Logo;