import React, { Component } from 'react';
import './Orders.css';
import axios from '../../axios-orders';
import OrderItem from '../../components/Order/OrderItem/OrderItem';
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";

class Orders extends Component {

    state = {
        loading: true,
        orders: []
    };

    componentDidMount() {
        axios.get('/orders.json')
            .then(response => {
                const fetchedOrders = [];
                for (let key in response.data) {
                    fetchedOrders.push({...response.data[key], id: key});
                }
                this.setState({orders: fetchedOrders, loading: false})
            })
            .catch(() => {
                this.setState({loading: false});
            })
    }

    render() {
        let orders = this.state.orders.map(order => (
                <OrderItem key={order.id}
                           ingredients={order.ingredients}
                           price={order.price}
                />
        ));

        return orders
    }
}

export default withErrorHandler(Orders, axios)
