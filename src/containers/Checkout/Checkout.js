import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import CheckoutSummary from '../../components/Order/CheckoutSummary/CheckoutSummary';
import { Route } from 'react-router-dom';
import ContactData from './ContactData/ContactData';


class Checkout extends Component {

    checkoutCancelledHandler() {
        this.props.history.goBack();
    }

    checkoutContinuedHandler() {
        this.props.history.replace('/checkout/contact-data');
    }

    render() {
        return (
            <Fragment>
                <CheckoutSummary
                    ingredients={this.props.ingredients}
                    checkoutCancelledHandler={this.checkoutCancelledHandler.bind(this)}
                    checkoutContinuedHandler={this.checkoutContinuedHandler.bind(this)}
                />
                <Route
                    path={this.props.match.path + '/contact-data'}
                    component={ContactData}
                />
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        ingredients: state.ingredients
    }
};

export default connect(mapStateToProps)(Checkout);